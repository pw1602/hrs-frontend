export class User {
  constructor(
    public id?: number,
    public name?: string,
    public surname?: string,
    public email?: string,
    public login?: string,
    public password?: string
  ) {}
}
